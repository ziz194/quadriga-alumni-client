
let SERVER_URL_DEFAULT:string = 'http://192.168.10.10/api/';
let SERVER_PUBLIC_URL_DEFAULT:string = 'http://192.168.10.10/';
let IMAGE_URL_DEFAULT:string = 'http://192.168.10.10/img/user_generated/';
let DOCS_URL_DEFAULT:string = 'http://192.168.10.10/docs/';
let SITE_TITLE_DEFAULT:string = 'Quadriga Alumni Plattform';

// if (typeof app === 'undefined' || app === null) {
//     let app = { environment: 'test' };
// } else {

    if (process.env.ENV  === 'build:staging') {
        SERVER_URL_DEFAULT = 'https://staging-api.alumni.quadriga-hochschule.com/api/';
        SERVER_PUBLIC_URL_DEFAULT = 'https://staging-api.alumni.quadriga-hochschule.com/';
        IMAGE_URL_DEFAULT = 'https://staging-api.alumni.quadriga-hochschule.com/img/user_generated/';
        DOCS_URL_DEFAULT = 'https://staging-api.alumni.quadriga-hochschule.com/docs/';

    }

    if (process.env.ENV === 'build:production') {
        SERVER_URL_DEFAULT = 'https://api.quadriga-circle.com/api/';
        SERVER_PUBLIC_URL_DEFAULT = 'http://api.quadriga-circle.com/';
        IMAGE_URL_DEFAULT = 'https://api.quadriga-circle.com/img/user_generated/';
        DOCS_URL_DEFAULT = 'https://api.quadriga-circle.com/docs/';
    }
// }
export var SERVER_URL = SERVER_URL_DEFAULT;
export var SERVER_PUBLIC_URL = SERVER_PUBLIC_URL_DEFAULT ;
export var IMAGE_URL = IMAGE_URL_DEFAULT;
export var DOCS_URL = DOCS_URL_DEFAULT;
export var FEED_URL = 'https://www.quadriga-hochschule.com/rss.xml';
export var SITE_TITLE = SITE_TITLE_DEFAULT;
