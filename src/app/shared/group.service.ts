import { Injectable, OnInit } from '@angular/core';
import { SERVER_URL } from '../../config';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQAPGroup} from '../shared/interfaces';

    let propertiesURL = SERVER_URL + 'groups';
@Injectable()
export class GroupService implements OnInit {

    public groups;
    public group;

    constructor(private http: Http, private authHttp: AuthHttp, public store: Store<AppState>) {
        this.groups = [];
    }

    ngOnInit() {
        this.findAll();
    }

    findAll() {

        this.groups = this.authHttp.get(propertiesURL).map(this.extractData).catch(this.handleError);
        return this.groups;

    }

     findPrososal() {

        this.groups = this.authHttp.get(propertiesURL +  '?status=prosposal').map(this.extractData).catch(this.handleError);
        return this.groups;

    }


    findOne(id: number) {

        this.group = this.http.get(propertiesURL + '/' + id).map(this.extractData).catch(this.handleError);
        return this.group;

    }


    insertOne(message: IQAPGroup, formData) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.post(propertiesURL, JSON.stringify(message), options).map(this.extractData).toPromise();

    }

    delete(id: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.delete(propertiesURL + '/' + id, options).toPromise();

    }

    private extractData(res: Response) {

        let body = res.json();
        return body.data || {};
    }

      attend(id: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
       // console.log(localStorage.getItem('auth_token'));
        return this.authHttp.post(propertiesURL + '/' + id + '/attend', '', { headers }).toPromise();

    }

    cancelAttendance(id: number, userid: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.authHttp.delete(propertiesURL + '/' + id + '/attend/' + userid , { headers } ).toPromise();

    }

  

    handleError(error) {
     
        return Observable.throw(error.json().error || 'Server error');
    
    }

}
