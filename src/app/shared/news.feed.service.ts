import { Injectable} from '@angular/core';
import { Headers, RequestOptions, Http, Response} from '@angular/http';
import { AuthHttp} from 'angular2-jwt';
import { Observable} from 'rxjs/Observable';
import { Feed } from './interfaces'

@Injectable()
export class NewsFeedService {

    private rssToJsonServiceBaseUrl: string = 'https://rss2json.com/api.json?rss_url=';


    constructor(private http: Http) {
    }

    getFeedContent(url: string): Observable<Feed> {
        return this.http.get(this.rssToJsonServiceBaseUrl + url)
            .map(this.extractFeeds)
            .catch(this.handleError);
    }

    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    private extractFeeds(res: Response): Feed {
        let feed = res.json();
        return feed || { };
    }


}
