// Check if the Token of the user is still valid
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { AppState } from '../shared/interfaces';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { Store } from '@ngrx/store'

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private router: Router, private store: Store<AppState>) {}

  canActivate() {
    //Token Information
    console.log (this.auth.useJwtHelper())
    //
    if (this.auth.isExpired()) {
        console.log (this.auth.isExpired());
        this.router.navigate(['/']);
        localStorage.setItem('auth_token', '');
        localStorage.setItem('user', '');
        this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: "" });
        jQuery('#sessionExpiredModal').openModal();
      return false;
    } else {
      return true;
    }
  }
}
