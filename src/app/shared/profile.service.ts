import { Injectable} from '@angular/core';
import { SERVER_URL} from '../../config';
import { Headers, RequestOptions, Http, Response} from '@angular/http';
import { AuthHttp} from 'angular2-jwt';
import { Observable} from 'rxjs/Observable';
import { IQAPUserProfile } from '../shared/interfaces';


let propertiesURL = SERVER_URL + 'users' ;

@Injectable()
export class ProfileService {

    public profiles;
    public profile;

    constructor(private http: Http, public authHttp: AuthHttp) {
        this.profiles = [];
    }

    findAll() {
        this.profiles = this.authHttp.get(propertiesURL).map(this.extractData).catch(this.handleError);
        return this.profiles;
    }

    findOne(id: number) {

        this.profile = this.authHttp.get(propertiesURL + '/' + id).map((res) => this.extractData(res)).catch(this.handleError);
        return this.profile;

    }

    getBookmarks(profileId: number) {

        return this.authHttp.get(propertiesURL + '/' + profileId + '/bookmark/').toPromise();

    }

    addBookmark(profileId: number, eventId: number) {

        return this.authHttp.post(propertiesURL + '/' + profileId + '/bookmark/', JSON.stringify({ event: eventId })).toPromise();

    }

    delBookmark(profileId: number, eventId: number) {

        return this.authHttp.delete(propertiesURL + '/' + profileId + '/bookmark/' + eventId ).toPromise();

    }

    updateOne(profile: IQAPUserProfile, formData) {
        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.put(propertiesURL + '/' + profile.id, JSON.stringify(profile), options).map((res) => this.extractData(res)).catch(this.handleError);

    }

    create(email: string, password: string) {

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        // console.log(JSON.stringify({ email: email, password: password })); 
        return this.authHttp.post(propertiesURL, JSON.stringify({ email: email, password: password }), options).toPromise();

    }

    private extractData(res: Response) {

        let body = res.json();
        return body.user || body.data;
    }

    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
