// Check JWT token service
import { Injectable } from '@angular/core';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS, JwtHelper} from 'angular2-jwt';
import { SERVER_URL} from '../../config';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from '../shared/interfaces';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';


let propertiesURL = SERVER_URL ;

@Injectable()
export class AuthService  {

    private jwtHelper: JwtHelper = new JwtHelper();
    private token = "" ; 

    constructor(private http: Http, public authHttp: AuthHttp, public store: Store<AppState>) {
    }

    useJwtHelper() {
        this.token  = localStorage.getItem('auth_token');
        return " //Token Expiration Date :  " + this.jwtHelper.getTokenExpirationDate(this.token) + " //Is Token Expired : " + this.jwtHelper.isTokenExpired(this.token);
    }
    isExpired() {
        this.token = localStorage.getItem('auth_token');
        return this.jwtHelper.isTokenExpired(this.token);
    }

    authenticate(email, password) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.http.post(propertiesURL + 'auth', JSON.stringify({ email: email, password: password }), {
            headers: headers
        });
    }

    user () {
        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        return this.authHttp.get(propertiesURL + 'auth/user', headers);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.user || body.data;
    }

   sendResetMail(mail) {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.post(propertiesURL + 'forgot-password', JSON.stringify({ email: mail }), options).toPromise();

    }

    sendNewPassword(password, token) {

        let headers = new Headers();
        headers.append('Content-Type', 'text/json');
        headers.append('X-XSRF-TOKEN', localStorage.getItem('auth_token'));
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.post(propertiesURL + 'reset-password', JSON.stringify({ token: token, password: password, password_repeat: password }), options).toPromise();

    }
    


}





