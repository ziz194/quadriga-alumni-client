import { Injectable} from '@angular/core';
import { SERVER_URL} from '../../config';
import {IQAPApplicationDefault } from '../shared/interfaces'
import { Headers, RequestOptions, Http, Response} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

let favorites = [],

    settingsURL = SERVER_URL + 'settings'

@Injectable()
export class SettingService {

    public settings;

    constructor(private http: Http, public authHttp: AuthHttp) {
        this.settings = [];
    }

findSettings() {
        this.settings = this.authHttp.get(settingsURL).map((res) => this.extractData(res)).catch(this.handleError);
        return this.settings;
}

updateSettings(settings: IQAPApplicationDefault, formData) {
    let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.put(settingsURL + '/1', JSON.stringify(settings), options).toPromise();
}


handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
}

private extractData(res: Response) {

        let body = res.json();
        return body.user || body.data;
}



}