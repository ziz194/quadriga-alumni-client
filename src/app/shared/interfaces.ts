import { Observable }  from 'rxjs/Rx';

export interface AppState {

    counter: Observable<number>;
    qapevent: Observable<IQAPEvent>;
    qapeventitems: Observable<IQAPEventItems>;
    qapprofileitems: Observable<IQAPUserProfileItems>;
    qapeventbookmarks: Observable<IQAPBookmarks>;
    qapfaqitems: Observable<IQAPFaqItems>;
    qapimprint: Observable<IQAPImprint>;

}

// RSS Feed Interfaces
export interface Feed {
    items:any ; 
}

export interface FeedInfo {

}

export interface FeedEntry {

}

export interface IQAPEvent {
    id: number;
    title: string;
    type: string;
    user: IQAPUserProfile;
    attendees: any[];
    isBookmarked: boolean;
    min_attendees: number;
    max_attendees: number;
    description: string;
    tag_ballsport: boolean;
    tag_wassersport: boolean;
    tag_teamsport: boolean;
    tag_fitness: boolean;
    tag_raeder: boolean;
    tag_handwerken: boolean;
    tag_gesundheit: boolean;
    tag_kultur: boolean;
    tag_sprachen: boolean;
    tag_sonstiges: boolean;
    tags: string;
    start_date: string;
    start_time: string;
    end_date: string;
    endtime: string;
    repeat_mode: string;
    repeat_end_daily: string;
    repeat_cycle_weekly: string;
    repeat_end_weekly: string;
    repeat_day_weekly: string;
    repeat_cycle_monthly: string;
    repeat_end_monthly: string;
    repeat_day_monthly: string;
    repeat_cycle_yearly: string;
    repeat_end_yearly: string;
    repeat_day_yearly: string;
    repeat_day0_custom: string;
    repeat_day1_custom: string;
    repeat_day2_custom: string;
    repeat_day3_custom: string;
    repeat_day4_custom: string;
    repeat_day5_custom: string;
    host_company: string;
    host_street: string;
    host_house_number: string;
    host_zip: string;
    host_city: string;
    host_details: string;
    comments: IQAPCommentItems[]; 

}

export interface IQAPEventItems extends Array<IQAPEvent>{

}

export interface IQAPUserProfile {
    
   id: number;
    title:string;
    email:string;
    firstname: string;
    lastname: string;
    suffix: string;
    about: string;
    xing: string;
    linkedin: string;
    facebook: string;
    company: string;
    company_street: string;
    company_house_number: string;
    company_zip: string;
    company_city: string;
    career_level: string;
    avatar: Array<any>;
    user_events: Array<any>;
    mail_notifications: boolean; 
    bookmarks:string;
    
}

export interface IQAPMessage {

    id:number;
    subject:string;
    text:string;
    user: IQAPUserProfile;
    recipients: string[];
    status:string ;
}

export interface IQAPGroup {
    id:number;
    title:string;
    description:string;
    user: IQAPUserProfile;
    users: any[];
}
export interface IQAPUserProfileItems extends Array<IQAPUserProfile>{

}

export interface IQAPGroupItems extends Array<IQAPUserProfile>{

}

export interface IQAPMessageItems extends Array<IQAPUserProfile>{

}
export interface IQAPBookmarks extends Array<Number> {

}

export interface IQAPFaq {
    question: string;
    answer: string;
}

export interface IQAPFaqItems extends Array<IQAPFaq>{

}

export interface IQAPImprint {
    html: string;
}

export interface IQAPNewComment {
    
    event: number;
    content:string;
    
}

export interface IQAPComment {
    
    id: number;
    event: number;
    date:string;
    user: IQAPUserProfile;
    content:string;
    
}

export interface IQAPCommentItems extends Array<IQAPComment>{

}

export interface IQAPApplicationDefault {

    id: number;

    // Mail Settings
    mail_html_template:string;
    mail_from:string;
    mail_from_descr:string;
    reply_to:string;

    // Mails Content
    attendee_confirmation:string;
    attendee_confirmation_subject:string;
    attendee_event_change:string;
    attendee_event_change_subject:string;
    attendee_event_reminder:string;
    attendee_event_reminder_subject:string;
    attendee_rating_reminder:string;
    attendee_rating_reminder_subject:string;
    host_event_reminder:string;
    host_event_reminder_subject:string;
    host_rating_reminder:string;
    host_rating_reminder_subject:string;
    contact:string;
    contact_subject:string;
    contact_reply:string;
    contact_reply_subject:string;
    event_delete:string;
    event_delete_subject:string;
    host_attendee_info:string;
    host_attendee_info_subject:string;
    host_attendee_limit:string;
    host_attendee_limit_subject:string;
    password:string;
    password_subject:string;
    user_registration:string;
    user_registration_subject:string;
    create_event_mail_confirmation_text:string;
    create_event_mail_confirmation_text_subject:string;
    
    // Default form Values
    default_event_location:string;
    default_event_city:string;
    default_event_zip:string;
    default_event_street:string;
    default_event_number:string;   
    talk_keywords:string;
    workshop_keywords:string;
    freizeit_keywords:string;
    occupational_areas:string;
    career_levels:string;

}
