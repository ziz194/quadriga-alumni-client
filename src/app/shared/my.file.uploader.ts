import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
export class MyFileUploader extends FileUploader {
  onAfterAddingFile(file: any) {
    file.withCredentials = false;
  }
}