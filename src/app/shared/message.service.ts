import { Injectable, OnInit } from '@angular/core';
import { SERVER_URL } from '../../config';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState, IQAPMessage } from '../shared/interfaces';

    let propertiesURL = SERVER_URL + 'messages';
@Injectable()
export class MessageService implements OnInit {

    public messages;
    public message;

    constructor(private http: Http, private authHttp: AuthHttp, public store: Store<AppState>) {
        this.messages = [];
    }

    ngOnInit() {
        this.findAll();
    }

    findAll() {

        this.messages = this.authHttp.get(propertiesURL).map(this.extractData).catch(this.handleError);
        return this.messages;

    }

     findPrososal() {

        this.messages = this.authHttp.get(propertiesURL +  '?status=prosposal').map(this.extractData).catch(this.handleError);
        return this.messages;

    }


    findOne(id: number) {

        this.message = this.http.get(propertiesURL + '/' + id).map(this.extractData).catch(this.handleError);
        return this.message;

    }

    markAsRead(id: number) {

        this.message = this.http.get(propertiesURL + '/markasread/' + id).map(this.extractData).catch(this.handleError);
        return this.message;

    }



    insertOne(message: IQAPMessage, recipients: string[], formData) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        message.recipients = recipients ; 
        return this.authHttp.post(propertiesURL, JSON.stringify(message), options).map(this.extractData).toPromise();

    }

    delete(id: number) {

        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let options = new RequestOptions({ headers: headers });
        return this.authHttp.delete(propertiesURL + '/' + id, options).toPromise();

    }

    private extractData(res: Response) {

        let body = res.json();
        return body.data || {};
    }

  

    handleError(error) {
     
        return Observable.throw(error.json().error || 'Server error');
    
    }

}
