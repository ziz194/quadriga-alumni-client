import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(value: any, args: string[]): any {
        let filter = args[0];

        if (filter && Array.isArray(value)) {
            let filterKeys = Object.keys(filter);
            return value.filter(item =>
                filterKeys.reduce((memo, keyName) =>
                memo && item[keyName] === filter[keyName], true));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

    transform(value: String) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
    }
}


@Pipe({
    name: 'qapsplitjoin'
})
export class SplitJoin implements PipeTransform {

    transform(value: String, split: string = ', ', join: string = ',  ') {
        if (value) {
            return value.split(split).join(join);
        }
    }
}



@Pipe({
    name: 'qapeventtypesearch'
})
export class EventTypeSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            var typeSearch: string;
            typeSearch = search.toLowerCase();
            switch (typeSearch) {
                case "freizeit":
                    return value.filter((item) => (item.type == 'freestyle'));
                case "talk":
                    return value.filter((item) => (item.type == 'talk'));
                case "workshops":
                    return value.filter((item) => (item.type == 'workshop'));
            }
        } else {
            return value;
        }
    }
}



@Pipe({
    name: 'qapsplitwrapjoin'
})
export class SplitWrapJoin implements PipeTransform {

    transform(value: String, split: String = ',', wrapBefore: string = '<div class="tag">#', wrapAfter: string = ' </div>', joinThis: string = '') {
        if (value) {
            var newVal = '';
            var arrVal = value.split(',');
            arrVal.forEach(element => {
                newVal += wrapBefore + element + wrapAfter + joinThis;
            });
            return newVal;

        }
    }
}

@Pipe({
    name: 'qapeventsearch'
})
export class EventSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            console.log(search);
            return value.filter((item) => (
                item.description.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.title.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.host_city.toLowerCase().indexOf(search.toLowerCase()) != -1
            ));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qapeventtopicsearch'
})
export class EventTopicSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (item.topic.toLowerCase().indexOf(search.toLowerCase()) != -1));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qapremoveduplicate'
})
export class RemoveDuplicate implements PipeTransform {

    transform(value: any){

        function removeDuplicates(originalArray, prop) {
            var newArray = [];
            var lookupObject  = {};
            for(var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }
            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }
        var uniqueArray = removeDuplicates(value, "host_city");
        return uniqueArray;

    }
}

@Pipe({
    name: 'qapeventregionsearch'
})
export class EventRegionSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (item.host_city.indexOf(search) != -1));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qapeventuseridsearch'
})
export class EventUserIdSearch implements PipeTransform {

    transform(value, search: number = 0) {
        if (search != 0) {
            return value.filter((item) => (item.user.id == search));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qapeventattendeeidsearch'
})
export class EventAttandeeIdSearch implements PipeTransform {


    transform(value, search: number = 0) {
        let found: boolean = false;

        if (value != null && search != 0) {
            for (var e = 0; e < value.length; e++) {
                var element = value[e];
                if (element.id === search) {
                    found = true;
                }
            }
            return found;
        } else {
            return found;
        }
    }
}




@Pipe({
    name: 'qapprofiletopicsearch'
})
export class ProfileTopicSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (item.occupational_area.toLowerCase().indexOf(search.toLowerCase()) != -1));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'messagefilterpipe'
})
export class MessageFilterPipe implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (item.status.toLowerCase().indexOf(search.toLowerCase()) != -1));
        } else {
            return value;
        }
    }
}

@Pipe({
    name: 'qapprofilesearch'
})
export class ProfileSearch implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            console.log('->  '+search);
            return value.filter((item) => (
                item.firstname.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.lastname.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.company.toLowerCase().indexOf(search.toLowerCase()) != -1
            ));
        } else {
            return value;
        }
    }
}