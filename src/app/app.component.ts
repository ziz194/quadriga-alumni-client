import { Component } from '@angular/core';
import { Router } from '@angular/router';

import '../style/app.scss';

@Component({
  selector: 'qap-app', // <my-qap></my-qap>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public activeRoute : string ;
  private selectedMenu: number ;
  constructor() {

  }
}

