import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { SettingService } from '../shared/setting.service';
import { GroupService } from '../shared/group.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE, SETEVENTITEMS, SETEVENT, INCREMENT, DECREMENT, RESET, SETVAL , ADD_EVENT_BOOKMARK, DEL_EVENT_BOOKMARK, UPDATE_EVENTITEM } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import { AppState,  IQAPUserProfile, IQAPEvent, IQAPApplicationDefault, IQAPEventItems, IQAPBookmarks, IQAPGroup, IQAPGroupItems } from '../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterializeDirective } from 'angular2-materialize';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventTypeSearch, EventRegionSearch, RemoveDuplicate } from '../shared/pipe.filters';
import { MomentModule } from 'angular2-moment';
import { SITE_TITLE } from '../../config';

@Component({
    selector: 'qap-group',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.scss'],
})

export class GroupComponent {
    public qapgroup: Observable<IQAPGroup>;
    public qapgroupitems: Observable<IQAPGroup>;
    private groups;
    
    private IMAGE_URL = IMAGE_URL;

    
    constructor(public store: Store<AppState>,
                private titleService: Title,
                private groupService: GroupService,
                private router: Router,
    ) {
        this.titleService.setTitle(SITE_TITLE);
        this.getGroups();
    }

    public setTitle(newTitle: string) {
        //this.titleService.setTitle(newTitle);
    }

    private getGroups() {
        this.groupService.findAll().subscribe(
            data => {
                this.groups = data ; 
            },
            err => { console.log(err); }
        );
    }
}