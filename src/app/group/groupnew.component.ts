import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { ProfileService } from '../shared/profile.service';
import { GroupService } from '../shared/group.service';
import { SettingService } from '../shared/setting.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import { AppState, IQAPGroup, IQAPApplicationDefault } from '../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterializeDirective } from 'angular2-materialize';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';

@Component({
    selector: 'qap-groupnew',
    templateUrl: './groupnew.component.html',
    styleUrls: ['./group.component.scss']
})

export class GroupNewComponent {
    private groupForm: FormGroup;
    private group: IQAPGroup;
    private queueItem = 0 ;
    public uploader: MyFileUploader = new MyFileUploader({ url: SERVER_URL + 'uploadFile'}) ;
    constructor(
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public groupService: GroupService,
        private router: Router
    ) {

        this.createForm();
    }
    
    ngOnInit() {
        
    }

    createForm() {

        this.groupForm = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required]

        });

    }

    public enableConfirm(): void {
        $('#bild_upload_button').prop('disabled', false);
    }


    onSubmit(): void {

        if (this.groupForm.valid) {

            Materialize.toast('Deine Gruppe wird erstellt...', 4000, 'rounded');

            let multipartform = new FormData();

            this.groupService.insertOne(this.groupForm.value, multipartform).then(
                result => {
                        this.uploader.onBuildItemForm = (item, form) => {
                            form.append("group", result.id);
                            form.append("description", 'groupPhoto');
                        };

                        this.uploader.queue[this.queueItem].upload();
                        Materialize.toast('Das Eventbild ist hochgeladen!', 4000, 'green rounded');
                        $('#copyright_confirm_event').prop('disabled', true);
                        this.uploader.onSuccessItem = (item:any, response:any, status:any, headers:any) => {
                            this.uploader.clearQueue();   
                        }
                    Materialize.toast('Deine Gruppe wurde erstellt', 4000, 'green rounded');
                    this.router.navigate(['/group']);

                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red rounded');
                    return false;
                }
            );

        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }
    }

   
}

