import { Component, Input, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Observable}  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { AppState, IQAPGroup, IQAPGroupItems, IQAPUserProfile, IQAPComment, IQAPCommentItems, IQAPNewComment } from '../shared/interfaces'
import { SETEVENT, DEL_EVENT_BOOKMARK, ADD_EVENT_BOOKMARK, UPDATE_EVENTITEM, SET_CURRENT_USER_PROFILE } from '../shared/state.actions'
import { GroupService } from '../shared/group.service';
import { IMAGE_URL } from '../../config';
import { Title } from '@angular/platform-browser';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventAttandeeIdSearch} from '../shared/pipe.filters'
import { MaterializeDirective } from "angular2-materialize";
import { CustomValidators } from '../shared/validators';
import { ProfileService } from '../shared/profile.service';
import { SITE_TITLE } from '../../config';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { SERVER_URL} from '../..//config';
import { DOCS_URL} from '../../config';
import { AuthGuardService } from '../shared/auth.guard.service';
import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/de';

const API_URL = SERVER_URL + 'uploadFile';

@Component({
    selector: 'qap-group-detail',
    templateUrl: './groupdetail.component.html', 
    styleUrls: ['./group.component.scss'],
})

export class GroupDetailComponent {

    private mailForm: FormGroup;
    private commentForm: FormGroup;
    public group: Observable<IQAPGroup>;
    public qapuserprofile: Observable<IQAPUserProfile>;
    private profile: IQAPUserProfile;
    public qapcommentitems: Observable<IQAPCommentItems>;
    public events_error: Boolean = false;
    public notFound: Boolean = false;
    private isAttended: Boolean = false;
    public IMAGE_URL = IMAGE_URL;
    private SERVER_URL = SERVER_URL;
    public currentgroup: IQAPGroup;
    public currentuser;
    public currentUserIsOwner: boolean = false;
    private id: number;
    private cancel_user_id: number;
    public showComment: Boolean = true;
    private DOCS_URL = DOCS_URL;

    constructor(
        public groupService: GroupService,
        public store: Store<AppState>,
        private router: Router,
        private route: ActivatedRoute,
        private titleService: Title,
        private formBuilder: FormBuilder,
        private eventAttandeeIdSearch: EventAttandeeIdSearch,
        public profileService: ProfileService,
    ) {

        moment.locale('de');
        this.isAttended = false;
        window.scrollTo(0,0);
        this.group = this.store.select<IQAPGroup>('group');
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
        this.qapcommentitems = store.select<IQAPCommentItems>('qapcommentitems');
        this.isAttended = false;
        this.mailForm = formBuilder.group({
            subject: ['', Validators.required],
            message: ['', Validators.required]
        });

        this.commentForm = formBuilder.group({
            content: ['', Validators.required]
        });

        this.qapuserprofile.subscribe( data => 

            this.profileService.findOne(data.id).subscribe(
                data => {
                                this.profile = data;
                                this.cancel_user_id = this.profile.id ;
                                console.log('USER ', data);
                                // this.isAttended = this.eventAttandeeIdSearch.transform(this.currentgroup.users, data.id);

                            }

        ));

        this.route.params.subscribe(params => {
            this.id = +params['id'];
            this.groupService.findOne(this.id)
                .subscribe(
                data => {
                    this.currentgroup = data;
                    this.store.dispatch({ type: SETEVENT, payload: data })
                    this.titleService.setTitle(this.currentgroup.title);
                    this.qapuserprofile.subscribe(data => {
                        if (this.currentgroup.user.id === data.id) {
                            this.currentUserIsOwner = true;
                        }
                        this.profileService.findOne(data.id).subscribe(
                            data => {
                                this.profile = data;
                            }
                        );
                        this.isAttended = this.eventAttandeeIdSearch.transform(this.currentgroup.users, data.id);

                    
                });

                },

                err => {
                    this.notFound = true;
                    Materialize.toast('Die Gruppe konnte nicht gefunden werden', 4000, 'red');
                }
                )
        });

    }



    attendGroup() {

        if (this.isAttended === true) {

        }
        this.groupService.attend(this.id).then(
            data => {
                let result = data.json();
                Materialize.toast(result.message, 4000, 'green rounded');
                this.isAttended = true;
                this.groupService.findOne(this.id).subscribe(
                    data => {
                        this.currentgroup = data;
                    }
                );
            }).catch(
            error => {
                let data = error.json();
                Materialize.toast(data.error.message, 4000, 'red rounded');
            }
            )

    }


    showDeleteModal() {
        jQuery('#modalDeleteGroup').openModal();
    }

    deleteGtoup(confirm: boolean) {
        if (confirm) {
            this.groupService.delete(this.id).then(
                result => {

                    jQuery('#modalDeleteGroup').closeModal();
                    Materialize.toast('Deine Gruppe wurde gelöscht', 4000, 'green rounded');
                    this.router.navigate(['/group']);
                }
            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.', 4000, 'red rounded')
                    return false;
                }
                );
        }
        else jQuery('#modalDeleteGroup').closeModal();

    }


        cancelAttendanceGroup(group: Event) {
        
      
        jQuery('#modalCancelAttendanceGroup').closeModal();
        this.groupService.cancelAttendance(this.id,this.cancel_user_id).then(

            data => {

                let result = data.json();
                this.isAttended = false;
                  
                Materialize.toast(result.message, 4000, 'green rounded');
                document.getElementById('user' +this.cancel_user_id.toString()).remove();

                this.groupService.findOne(this.id).subscribe(
                    data => {
                        this.currentgroup = data;
                    }
                );
                            }

        ).catch(

            error => {
                console.log('error', error);
                //let data = error.json();// data.error.message
                Materialize.toast('error', 4000, 'red rounded');
            }

            );
    }

    showcancelAttendanceGroup(group: Event, userid: number){
        jQuery('#modalCancelAttendanceGroup').openModal();
        this.cancel_user_id = userid;
        console.log (this.cancel_user_id)
    }
}