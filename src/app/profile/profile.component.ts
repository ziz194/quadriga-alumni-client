import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute  } from '@angular/router';
import {RouteConfig, RouteParams, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, CanActivate} from 'angular2/router'
import { Observable }  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { AppState,  IQAPUserProfile} from '../shared/interfaces';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { ProfileService } from '../shared/profile.service';
import { EventService } from '../shared/event.service';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';

@Component({
  selector: 'qap-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

    public qapuserprofile: Observable<IQAPUserProfile>;
    public isRequesting: boolean;
    private currentuser;
    private currentUserLoggedIn: Boolean = false;
    public events_error: Boolean = false;
    public currentbookmarks;
    private IMAGE_URL: string = IMAGE_URL;
    private SERVER_URL: string = SERVER_URL;
    
  constructor(
        private titleService: Title,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private profileService: ProfileService,
        private eventService: EventService,
        private router: Router
  ) {
        this.isRequesting = true;
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
        this.route.params.subscribe(params => {
             let group:number = 0;
            if(params['group']){
                let group:number = params['group'];
            }
            let id:number = params['id'];
            this.profileService.findOne(id).subscribe(
                data => {
                    this.currentuser = data;
                    this.titleService.setTitle(this.currentuser.firstname + ' ' + this.currentuser.lastname);
                    this.isRequesting = false;

                },
                err => {
                    this.events_error = true ;
                }
            );
            this.qapuserprofile.subscribe(
                data => {
                    if (data.id === id ) {
                        this.currentUserLoggedIn = true;
                    }
                }
            );
        });
  }

  ngOnInit() {
    jQuery('#sidebar').show() ; 
  }
}
