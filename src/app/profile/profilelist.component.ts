import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { SettingService } from '../shared/setting.service';
import { ProfileService } from '../shared/profile.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE, SETEVENTITEMS, SETEVENT, INCREMENT, DECREMENT, RESET, SETVAL , ADD_EVENT_BOOKMARK, DEL_EVENT_BOOKMARK, UPDATE_EVENTITEM } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import { AppState,  IQAPUserProfile, IQAPEvent, IQAPApplicationDefault, IQAPEventItems, IQAPBookmarks } from '../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterializeDirective } from 'angular2-materialize';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, ProfileTopicSearch, ProfileSearch } from '../shared/pipe.filters';
import { MomentModule } from 'angular2-moment';


@Component({
    selector: 'qap-member',
    templateUrl: './profilelist.component.html', 
    styleUrls: ['./profile.component.scss'],
    // pipes: [FilterPipe,CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch]
})

export class ProfileListComponent {
    public profiles;
    public topicSearch = '';
    public yearSearch = '';
    public topicSearchDefault: string = 'Alle Themen';
    public yearSearchDefault: string = 'Jahrgang';
    public tagSearchDefault: string = 'Schlagwort';
    public qapuserprofile: Observable<IQAPUserProfile>;
    private IMAGE_URL: string = IMAGE_URL;

    constructor(public store: Store<AppState>, 
       public profileService: ProfileService,
    ) {
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
        this.getProfiles() ;
    }
    
    private getProfiles() {
        this.profileService.findAll().subscribe(
            data => {
               this.profiles = data ; 
            },
            err => { console.log(err); }
        );
    }
    filterTopic(search){
        this.topicSearch = search;
        if (this.topicSearch === ''){
            this.topicSearchDefault = 'Fachgebiet';
        } else {
            this.topicSearchDefault = search;
        }
    }
     filterYear(search){
        this.yearSearch = search;
        if (this.yearSearch === ''){
            this.yearSearchDefault = 'Jahrgang';
        } else {
            this.yearSearchDefault = search;
        }
    }


}