import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable }  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { AppState,  IQAPUserProfile, IQAPApplicationDefault} from '../shared/interfaces';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { ProfileService } from '../shared/profile.service';
import { EventService } from '../shared/event.service';
import { SettingService } from '../shared/setting.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'qap-profileedit',
  templateUrl: './profileedit.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileEditComponent implements OnInit {

    private qapuserprofile: Observable<IQAPUserProfile>;
    private profileForm: FormGroup;
    private profile: IQAPUserProfile;
    public settingValues: IQAPApplicationDefault;
    private occupational_areas: string[]; ;
    private career_levels: string[]; 
    private queueItem = 0;
    public confirmShow: boolean = false ; 
    public uploader: MyFileUploader = new MyFileUploader({ url: SERVER_URL + 'uploadFile'}) ; 


      constructor(
        private formBuilder: FormBuilder,
        private store: Store<AppState>,
        private profileService: ProfileService,
        private titleService: Title,
        private router: Router,
        private route: ActivatedRoute,
        private settingService: SettingService,
        private authService: AuthService

      ) {
      this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
      this.settingService.findSettings().subscribe(
            data => {
                this.settingValues = data;
                this.career_levels = this.settingValues.career_levels.split(',') ;
                this.occupational_areas = this.settingValues.occupational_areas.split(',')  ;
                this.occupational_areas.forEach(tag => {
                    (this.profileForm.addControl(tag, new FormControl()));
                });
            },

      this.authService.user().subscribe(
            data => {
                let currentProfile = data.json();
                this.profile = currentProfile.user;
                if (this.profile['occupational_area'] != '') {
                    for (var item of this.profile['occupational_area'].split(',')) {
                        if (this.profileForm.get(item)) {
                            (<FormControl>this.profileForm.get(item)).setValue(1);
                        }
                    }
                }
                if( this.profile['mail_notifications'] == true ) { 
                     (<FormControl>this.profileForm.get('mail_notifications')).setValue(1);
                }
            }
        )
        );

        this.profileForm = formBuilder.group({
            id: ['', Validators.required],
            title: ['', Validators.nullValidator],
            firstname: ['', Validators.required],
            suffix: ['', Validators.nullValidator],
            lastname: ['', Validators.required],
            about: ['', Validators.nullValidator],
            xing: ['', Validators.nullValidator],
            linkedin: ['', Validators.nullValidator],
            facebook: ['', Validators.nullValidator],
            company: ['', Validators.required],
            company_street: ['', Validators.required],
            company_house_number: ['', Validators.required],
            company_zip: ['', Validators.required],
            company_city: ['', Validators.required],
            career_level: ['', Validators.nullValidator],
            avatar: ['', Validators.nullValidator],
            mail_notifications: [0, Validators.nullValidator],
            occupational_area: ['', Validators.nullValidator]
        });


        this.authService.user().subscribe(
            data => {
                let currentProfile = data.json();
                this.profile = currentProfile.user;
                if (this.profile['occupational_area'] != '') {
                    for (var item of this.profile['occupational_area'].split(',')) {
                        if (this.profileForm.get(item)) {
                            // console.log('Schlagwort: '+item) ;
                            (<FormControl>this.profileForm.get(item)).setValue(1);
                        }
                    }
                }
                (<FormControl>this.profileForm.get('id')).setValue(this.profile.id);
                (<FormControl>this.profileForm.get('title')).setValue(this.profile.title);
                (<FormControl>this.profileForm.get('firstname')).setValue(this.profile.firstname);
                (<FormControl>this.profileForm.get('suffix')).setValue(this.profile.suffix);
                (<FormControl>this.profileForm.get('lastname')).setValue(this.profile.lastname);
                (<FormControl>this.profileForm.get('about')).setValue(this.profile.about);
                (<FormControl>this.profileForm.get('xing')).setValue(this.profile.xing);
                (<FormControl>this.profileForm.get('linkedin')).setValue(this.profile.linkedin);
                (<FormControl>this.profileForm.get('facebook')).setValue(this.profile.facebook);
                (<FormControl>this.profileForm.get('company')).setValue(this.profile.company);
                (<FormControl>this.profileForm.get('company_street')).setValue(this.profile.company_street);
                (<FormControl>this.profileForm.get('company_house_number')).setValue(this.profile.company_house_number);
                (<FormControl>this.profileForm.get('company_zip')).setValue(this.profile.company_zip);
                (<FormControl>this.profileForm.get('company_city')).setValue(this.profile.company_city);
                (<FormControl>this.profileForm.get('career_level')).setValue(this.profile.career_level);
 
                this.profileForm.markAsTouched;
                this.profileForm.updateValueAndValidity(true);
                this.titleService.setTitle(this.profile.firstname + ' ' + this.profile.lastname);

            }
        );
  }

  ngOnInit() {
  }


  public enableConfirm(): void {
        this.confirmShow = true ;
       jQuery('#copyright_confirm').prop('disabled', false);
       jQuery('#copyright_confirm').prop('checked', false);

    }

    uploadFile() {
    this.uploader.onBuildItemForm = (item, form) => {
            form.append('user', this.profile.id);
            form.append('description', 'avatar');
    };

    if (this.uploader.queue[0].file.size > 3000000) {
        Materialize.toast('Das Bild ist zu groß, bitte wähle ein anderes Bild aus', 4000, 'red rounded');
        this.uploader.clearQueue();
    }
    else {
        this.uploader.queue[0].upload();
    }
    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
        Materialize.toast('Dein Profilbild ist hochgeladen!', 4000, 'green rounded');
        this.uploader.clearQueue();

    }
        
   jQuery('#copyright_confirm').prop('disabled', true);


  }

  checkForm(): boolean {

    if (this.profileForm.valid) {
        return true; 
    }else return false;
  }


      onSubmit(): void {

        let new_occupational_areas = [];
        for (var item of this.occupational_areas) {
            if (this.profileForm.controls[item].value) {
                new_occupational_areas.push(item);
            }
         }
        (<FormControl>this.profileForm.get('occupational_area')).setValue(new_occupational_areas.join());
        this.profileForm.updateValueAndValidity(true);
        if (this.profileForm.valid) {
            // Avatar validation
            if(jQuery('#userAvatar').val() == '' && this.profile.avatar['name'] == 'missing.png') {
                Materialize.toast('Bitte wähle ein Profilbild aus', 4000, 'red');
            }
            else if (jQuery('#copyright_confirm').prop('checked') == false &&jQuery('#copyright_confirm').prop('disabled') == false) {
                Materialize.toast('Bitte Bestätigen Sie dass Sie alle Rechte vom hochgeladenen Bild haben', 4000, 'red rounded');
            }
            else {
            // elsejQuery('#copyright_confirm').prop('checked',true)  ;
                Materialize.toast('Dein Profil wird gespeichert...', 4000, 'rounded');
                let multipartform = new FormData();
                this.profileService.updateOne(this.profileForm.value, multipartform)
                .subscribe(
                  data => {
                      this.profile = data ;
                      Materialize.toast('Dein Profil wurde gespeichert', 4000, 'green rounded');
                      this.router.navigate(['/profile/' + this.profile.id]);
                      this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data});
                  }
                );
  


            }
        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere deine Daten', 4000, 'red rounded');
        }
    }

}   
