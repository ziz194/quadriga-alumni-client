import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { ProfileService } from '../shared/profile.service';
import { MessageService } from '../shared/message.service';
import { SettingService } from '../shared/setting.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import { AppState, IQAPMessage, IQAPApplicationDefault } from '../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterializeDirective } from 'angular2-materialize';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';

@Component({
    selector: 'qap-groupnew',
    templateUrl: './messagenew.component.html',
    styleUrls: ['./message.component.scss']
})

export class MessageNewComponent {
    private messageForm: FormGroup;
    private message: IQAPMessage;
    public profiles;
    private recipients: string[] = [];
    public contactEmailPerson ='';
    constructor(
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public messageService: MessageService,
        public profileService: ProfileService,
        private router: Router
    ) {

        this.getProfiles() ; 
        this.createForm();
    }

    contactPerson(contact){

       this.contactEmailPerson += contact.firstname + ' ' + contact.lastname + ' ';
       this.recipients.push(contact.id);
       let contactIndex = this.profiles.indexOf(contact);   
       this.profiles.splice(contactIndex, 1)
       console.log (this.recipients) ; 
       //this.contactEmailPerson.focus();
   }

    
    ngOnInit() {
        
    }
    private getProfiles() {
        this.profileService.findAll().subscribe(
            data => {
               this.profiles = data ; 
            },
            err => { console.log(err); }
        );
    }

    createForm() {

        this.messageForm = this.formBuilder.group({
            subject: ['', Validators.required],
            text: ['', Validators.required]
        });

    }

    sendMessage(): void {

        if (this.recipients.length == 0){
            Materialize.toast('Bitte wähle ein Empfänger aus', 4000, 'red rounded');
        }
        else{

        if (this.messageForm.valid) {

            Materialize.toast('Deine Nachricht wird geschickt...', 4000, 'rounded');

            let multipartform = new FormData();

            this.messageService.insertOne(this.messageForm.value, this.recipients, multipartform).then(
                result => {
                    Materialize.toast('Deine Nachricht wurde geschickt', 4000, 'green rounded');
                    this.router.navigate(['/messages']);
                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red rounded');
                    return false;
                }
            );

        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red rounded');
        }
    }
    }

   
}

