import { Component, Input, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Observable}  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { AppState, IQAPMessage, IQAPGroupItems, IQAPUserProfile, IQAPComment, IQAPCommentItems, IQAPNewComment } from '../shared/interfaces'
import { SETEVENT, DEL_EVENT_BOOKMARK, ADD_EVENT_BOOKMARK, UPDATE_EVENTITEM, SET_CURRENT_USER_PROFILE } from '../shared/state.actions'
import { MessageService } from '../shared/message.service';
import { IMAGE_URL } from '../../config';
import { Title } from '@angular/platform-browser';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventAttandeeIdSearch} from '../shared/pipe.filters'
import { MaterializeDirective } from "angular2-materialize";
import { CustomValidators } from '../shared/validators';
import { ProfileService } from '../shared/profile.service';
import { SITE_TITLE } from '../../config';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { SERVER_URL} from '../..//config';
import { DOCS_URL} from '../../config';
import { AuthGuardService } from '../shared/auth.guard.service';
import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/de';

const API_URL = SERVER_URL + 'uploadFile';

@Component({
    selector: 'qap-message-detail',
    templateUrl: './messagedetail.component.html', 
    styleUrls: ['./message.component.scss'],
})

export class MessageDetailComponent {

    private messageForm: FormGroup;
    private recipients: string[] = [];
    public message: Observable<IQAPMessage>;
    public qapuserprofile: Observable<IQAPUserProfile>;
    private profile: IQAPUserProfile;
    public events_error: Boolean = false;
    public notFound: Boolean = false;
    public IMAGE_URL = IMAGE_URL;
    private SERVER_URL = SERVER_URL;
    public currentmessage: IQAPMessage;
    public currentuser;
    public currentUserIsOwner: boolean = false;
    private id: number;
    private cancel_user_id: number;
    public reply: Boolean = false;
    private DOCS_URL = DOCS_URL;

    constructor(
        public messageService: MessageService,
        public store: Store<AppState>,
        private router: Router,
        private route: ActivatedRoute,
        private titleService: Title,
        private formBuilder: FormBuilder,
        public profileService: ProfileService,
    ) {

        moment.locale('de');
        this.createForm();
        window.scrollTo(0,0);
        this.message = this.store.select<IQAPMessage>('message');
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');

        this.qapuserprofile.subscribe( data => 

            this.profileService.findOne(data.id).subscribe(
                data => {
                                this.profile = data;
                                this.cancel_user_id = this.profile.id ;

                            }

        ));

        this.route.params.subscribe(params => {
            this.id = +params['id'];
            
            this.messageService.findOne(this.id)
                .subscribe(
                data => {
                    this.messageService.markAsRead(this.id).subscribe() ;
                    this.currentmessage = data;
                        this.recipients.push(data.user_id); 
                    this.messageForm.get('subject').setValue(data.subject);
                    
                    this.qapuserprofile.subscribe(data => {
                        if (this.currentmessage.user.id === data.id) {
                            this.currentUserIsOwner = true;
                        }
                        this.profileService.findOne(data.id).subscribe(
                            data => {
                                this.profile = data;
                            }
                        );

                    
                });

                },

                err => {
                    this.notFound = true;
                    Materialize.toast('Die Nachricht könnte nicht gefunden werden', 4000, 'red');
                }
                )
        });

    }



    showReply() {
        this.reply = true ; 
    }

    createForm() {

        this.messageForm = this.formBuilder.group({
            subject: ['', Validators.required],
            text: ['', Validators.required]
        });

    }


    sendMessage(): void {

            if (this.recipients.length == 0){
                Materialize.toast('Bitte wähle ein Empfänger aus', 4000, 'red rounded');
            }
            else{

            if (this.messageForm.valid) {

                Materialize.toast('Deine Nachricht wird geschickt...', 4000, 'rounded');

                let multipartform = new FormData();

                this.messageService.insertOne(this.messageForm.value, this.recipients, multipartform).then(
                    result => {
                        Materialize.toast('Deine Nachricht wurde geschickt', 4000, 'green rounded');
                        this.router.navigate(['/messages']);
                    }

                ).catch(
                    error => {
                        Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red rounded');
                        return false;
                    }
                );

            } else {
                Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red rounded');
            }
        }
    }


    showDeleteModal() {
        jQuery('#modalDeleteMessage').openModal();
    }

    deleteMessage(confirm: boolean) {
        if (confirm) {
            this.messageService.delete(this.id).then(
                result => {

                    jQuery('#modalDeleteMessage').closeModal();
                    Materialize.toast('Die Nachricht wurde gelöscht', 4000, 'green rounded');
                    this.router.navigate(['/messages']);
                }
            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.', 4000, 'red rounded')
                    return false;
                }
                );
        }
        else jQuery('#modalDeleteMessage').closeModal();

    }
}