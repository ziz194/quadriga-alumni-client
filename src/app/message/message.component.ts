import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {SET_CURRENT_USER_PROFILE} from '../shared/state.actions';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import { Observable }  from 'rxjs/Rx';
import { ProfileService } from '../shared/profile.service';
import { MessageService } from '../shared/message.service';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { Title } from '@angular/platform-browser';
import { AppState, IQAPUserProfile, IQAPEvent, IQAPApplicationDefault, IQAPEventItems, IQAPBookmarks } from '../shared/interfaces';
import { MessageFilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventTypeSearch, EventRegionSearch, RemoveDuplicate } from '../shared/pipe.filters';

import {AuthService} from '../shared/auth.service'
@Component({
    selector: 'qap-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.scss']
})
export class MessageComponent {
        public qapuserprofile: Observable<IQAPUserProfile>;
        public currentuser;
        private messageStatusFilter: string ; 
        public profiles;
        private showSent: boolean ;
        private IMAGE_URL: string = IMAGE_URL;

    constructor(
        private titleService: Title,
        private store: Store<AppState>,
        private profileService: ProfileService,
        private messageService: MessageService
    ) {
        this.showSent = false ; 
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
        this.getProfiles() ;
        this.qapuserprofile.subscribe(
                data => {

                this.profileService.findOne(data.id).subscribe(
                    data => {
                        this.currentuser = data;
                        // this.titleService.setTitle('Inbox von ' + this.currentuser.firstname + ' ' + this.currentuser.lastname);
                    },
              
                );
                }
        )

        console.log (this.currentuser) ;

    }


    private getProfiles() {
        this.profileService.findAll().subscribe(
            data => {
               this.profiles = data ; 
            },
            err => { console.log(err); }
        );
    }

    filterMessage(search){
        this.messageStatusFilter = search;
        this.showSent = false;
    }

    showSentMessages(search){
        this.showSent = true;
    }
        
}

