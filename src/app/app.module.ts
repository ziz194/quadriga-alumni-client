import {NgModule, ApplicationRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {PasswordComponent} from './login/password.component';
import {RegisterComponent} from './login/register.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {ProfileComponent} from './profile/profile.component';
import {ProfileEditComponent} from './profile/profileedit.component';
import {ProfileService} from './shared/profile.service';
import {EventService} from './shared/event.service';
import {GroupService} from './shared/group.service';
import {MessageService} from './shared/message.service';
import {EventNewComponent} from './event/eventnew.component';
import {EventDetailComponent} from './event/eventdetail.component';
import {EventComponent} from './event/event.component';
import {GroupComponent} from './group/group.component';
import {GroupDetailComponent} from './group/groupdetail.component';
import {GroupNewComponent} from './group/groupnew.component';
import {ProfileListComponent} from './profile/profilelist.component';
import {MessageComponent} from './message/message.component';
import {MessageNewComponent} from './message/messagenew.component';
import {MessageDetailComponent} from './message/messagedetail.component';
import {AuthGuardService} from './shared/auth.guard.service';
import {AuthService} from './shared/auth.service';
import {SettingService} from './shared/setting.service';
import {NewsFeedService} from './shared/news.feed.service';
import {routing} from './app.routing';
import {AuthConfig, AuthHttp} from 'angular2-jwt';
import {MaterializeModule} from 'angular2-materialize';
import {removeNgStyles, createNewHosts} from '@angularclass/hmr';
import {provideStore, combineReducers} from '@ngrx/store';
import {APP_BASE_HREF} from '@angular/common';
import {Ng2PaginationModule} from 'ng2-pagination';
import {Observable}  from 'rxjs/Rx';
import {SpinnerComponent} from './spinner/spinner.component';
import {FileSelectDirective, FileDropDirective} from 'ng2-file-upload/ng2-file-upload';
import {
    FilterPipe,
    CapitalizePipe,
    SplitJoin,
    SplitWrapJoin,
    EventSearch,
    EventTopicSearch,
    EventTypeSearch,
    EventRegionSearch,
    EventAttandeeIdSearch,
    RemoveDuplicate,
    ProfileTopicSearch,
    MessageFilterPipe,
    ProfileSearch
} from './shared/pipe.filters';
import {
    counterReducer,
    eventReducer,
    detailsReducer,
    qapeventitemsReducer,
    qapuserprofileReducer,
    qapEventBookmarksReducer,
    qapeventprosposalsReducer
} from './shared/state.reducers';
import {compose} from '@ngrx/core/compose';
import {localStorageSync} from 'ngrx-store-localstorage';
import {LocalStorageService, LocalStorageSubscriber} from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';
import {MomentModule} from 'angular2-moment';
import * as moment from 'moment';
// TODO: this does not seem to be used.
//import * as locales from 'moment/min/locales';
moment.locale('DE-DE');

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        MaterializeModule,
        MomentModule,
        Ng2PaginationModule,
        routing
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        SidebarComponent,
        ProfileComponent,
        ProfileEditComponent,
        EventNewComponent,
        EventDetailComponent,
        EventComponent,
        GroupComponent,
        GroupDetailComponent,
        GroupNewComponent,
        ProfileListComponent,
        MessageComponent,
        MessageNewComponent,
        MessageDetailComponent,
        LoginComponent,
        PasswordComponent,
        RegisterComponent,
        SpinnerComponent,
        FileSelectDirective,
        FilterPipe, MessageFilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventRegionSearch, EventTypeSearch,EventAttandeeIdSearch, RemoveDuplicate, ProfileTopicSearch, ProfileSearch,
        FileDropDirective
    ],
    providers: [
        // { provide: XSRFStrategy, useValue: new CookieXSRFStrategy},
        {provide: APP_BASE_HREF, useValue: '/'},
        ProfileService,
        EventService,
        SettingService,
        AuthGuardService,
        NewsFeedService,
        AuthService,
        GroupService,
        MessageService,
        EventAttandeeIdSearch,

        provideStore(
            compose(localStorageSync(['qapuserprofile', 'qapeventitems', 'qapeventbookmarks', 'qapeventprosposals'], true),
                combineReducers
            )({
                details: detailsReducer,
                event: eventReducer,
                counter: counterReducer,
                qapeventitems: qapeventitemsReducer,
                qapuserprofile: qapuserprofileReducer,
                qapeventbookmarks: qapEventBookmarksReducer,
                qapeventprosposals: qapeventprosposalsReducer
            })
        ),
        {
            provide: AuthConfig, useValue: new AuthConfig({
            headerName: 'Authorization',
            headerPrefix: 'Bearer ',
            tokenName: 'auth_token',
            tokenGetter: (() => localStorage.getItem('auth_token')),
            globalHeaders: [{'Content-Type': 'application/json'}],
            noJwtError: true,
            noTokenScheme: true
        })
        },
        AuthHttp
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(public appRef: ApplicationRef) {
    }

    hmrOnInit(store) {
        console.log('HMR store', store);
    }

    hmrOnDestroy(store) {
        let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
        // recreate elements
        store.disposeOldHosts = createNewHosts(cmpLocation);
        // remove styles
        removeNgStyles();
    }

    hmrAfterDestroy(store) {
        // display new elements
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    }
}


// Calendar Options
jQuery(function () {
    jQuery.extend(jQuery.fn.pickadate.defaults, {
        monthsFull: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthsShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        weekdaysFull: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        weekdaysLetter: ['S', 'M', 'D', 'M', 'D', 'F', 'S'],
        today: '',
        close: 'Schließen',
        firstDay: 1,
        min: true,
        closeOnSelect: true,
        format: 'dd.mm.yyyy',
        formatSubmit: 'yyyy-mm-dd',
        clear: 'Löschen'
    });

});