import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { ProfileService } from '../shared/profile.service';
import { EventService } from '../shared/event.service';
import { SettingService } from '../shared/setting.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import { AppState,  IQAPUserProfile, IQAPEvent, IQAPApplicationDefault } from '../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterializeDirective } from 'angular2-materialize';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';

@Component({
    selector: 'qap-eventnew',
    templateUrl: './eventnew.component.html',
    styleUrls: ['./event.component.scss']
})

export class EventNewComponent {

    private eventForm: FormGroup;
    private IMAGE_URL = IMAGE_URL;
    private previewPicPath: string;
    private event: IQAPEvent;
    public viewstep: number = 1;
    public repeat: string = "none";
    public page_mode: string = "new";
    public dataLoaded: boolean = true;
    public defaultValues: IQAPApplicationDefault;
    public freizeit_keywords: string[];
    public talk_keywords: string[];
    public workshop_keywords: string[];
    public eventType: string = 'talk' ;
    public confirmShow: boolean = false ;
    private queueItem = 0 ;
    private docsQueueItem = 0;
    private repeatDays: string[] = [];
    private customTopics: string[] = [];
    public uploader: MyFileUploader = new MyFileUploader({ url: SERVER_URL + 'uploadFile'}) ;
    public docsUploader: MyFileUploader = new MyFileUploader({ url: SERVER_URL + 'uploadFile'}) ;
    public hasBaseDropZoneOver: boolean = false;
    
    constructor(
        private formBuilder: FormBuilder,
        public store: Store<AppState>,
        public eventService: EventService,
        public settingService: SettingService,
        private router: Router
    ) {

        this.repeatDays.length;
        this.createForm();
        this.initDefaults();

    }
    
    private initDefaults() {
        this.customTopics = [];
        this.settingService.findSettings().subscribe(
            data => {
                this.defaultValues = data;
                if (this.defaultValues.freizeit_keywords) {
                    this.freizeit_keywords = this.defaultValues.freizeit_keywords.split(",");
                    this.freizeit_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.workshop_keywords) {
                    this.workshop_keywords = this.defaultValues.workshop_keywords.split(",");
                    this.workshop_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }
                if (this.defaultValues.talk_keywords) {
                    this.talk_keywords = this.defaultValues.talk_keywords.split(",");
                    this.talk_keywords.forEach(tag => {
                        (this.eventForm.addControl(tag, new FormControl()));
                    });
                }

                (<FormControl>this.eventForm.get('host_company')).setValue(this.defaultValues.default_event_location);
                (<FormControl>this.eventForm.get('host_street')).setValue(this.defaultValues.default_event_street);
                (<FormControl>this.eventForm.get('host_city')).setValue(this.defaultValues.default_event_city);
                (<FormControl>this.eventForm.get('host_zip')).setValue(this.defaultValues.default_event_zip);
                (<FormControl>this.eventForm.get('host_house_number')).setValue(this.defaultValues.default_event_number);
            }
        );

    }

    ngOnInit() {
        
    }
    changeEventType() {
        this.customTopics = [];
    }

    uploadEventPicTemp() {
        // Set uploader description
        this.uploader.onBuildItemForm = (item, form) => {
            form.append("description", 'temp');
        };
        this.uploader.queue.forEach(item => {
            if (item.file.size > 3000000) {
                Materialize.toast('Das Bild ist zu groß', 4000, 'red');
                this.uploader.clearQueue();
            } else {
                item.upload();
                this.uploader.onSuccessItem = (item: any, response: any, status: any, headers: any) => {
                    if (response == 'NO') {
                        Materialize.toast('Das Bild ist zu Klein, bitte wähle ein anderes Bild aus', 4000, 'red');
                        this.uploader.clearQueue();

                    } 
                    else {
                        this.previewPicPath = IMAGE_URL + 'tmp/' + response;
                    }
                };
                this.queueItem = this.uploader.getIndexOfItem(item);
            }
        });

    }

    uploadEventPic() {
        this.uploader.queue[this.queueItem].upload();
        Materialize.toast('Das Eventbild ist hochgeladen!', 4000, 'green rounded');
        jQuery('#copyright_confirm_event').prop('disabled', true);
    }

    uploadEventDocs() {
        this.docsUploader.uploadAll();
    }


    // input.onchange = function () {
    //     alert(this.value);
    // };​

    clearForm() {

        this.dataLoaded = true; // form reset only possible until angular RC5
        this.viewstep = 1;
        (<FormControl>this.eventForm.get('title')).setValue('');
        (<FormControl>this.eventForm.get('description')).setValue('');
        (<FormControl>this.eventForm.get('start_date')).setValue('');
        (<FormControl>this.eventForm.get('start_time')).setValue('08:00');
        (<FormControl>this.eventForm.get('end_time')).setValue('16:00');
        (<FormControl>this.eventForm.get('host_company')).setValue(this.defaultValues.default_event_location);
        (<FormControl>this.eventForm.get('host_street')).setValue(this.defaultValues.default_event_street);
        (<FormControl>this.eventForm.get('host_city')).setValue(this.defaultValues.default_event_city);
        (<FormControl>this.eventForm.get('host_zip')).setValue(this.defaultValues.default_event_zip);
        (<FormControl>this.eventForm.get('host_house_number')).setValue(this.defaultValues.default_event_number);
        this.previewPicPath =  null ;
        this.uploader.clearQueue();
        this.docsUploader.clearQueue();
    }

    createForm() {

        this.eventForm = this.formBuilder.group({
            type: ['talk', Validators.required],
            min_attendees: ['6', Validators.required],
            max_attendees: ['12', Validators.required],
            title: ['', Validators.required],
            description: ['', Validators.required],
            start_date: ['', Validators.required],
            start_time: ['08:00', Validators.required],
            end_time: ['16:00', Validators.required],
            host_company: ['', Validators.required],
            host_street: ['', Validators.required],
            custom_days: ['', Validators.nullValidator],
            topic: ['', Validators.nullValidator],
            host_house_number: ['13', Validators.required],
            host_zip: ['', Validators.required],
            host_city: ['', Validators.required],
            host_details: ['', Validators.nullValidator]

        });

    }

    toggleType(type) {
        this.eventType = type ;
        console.log(type);
    }

    toogleTopic(e) {

        let value: string = e.target.id;
        if (e.target.checked) {
            this.customTopics.push( value );
        } else {
            this.customTopics.splice( this.customTopics.indexOf(value) );
        }

    }

    //Drag and drop file upload
    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }


    onSubmit(): void {

        if (this.eventForm.valid) {

            Materialize.toast('Dein Event wird erstellt...', 4000, 'rounded');

            let multipartform = new FormData();

            (<FormControl>this.eventForm.get('topic')).setValue(this.customTopics.join(','));
            (<FormControl>this.eventForm.get('type')).setValue(this.eventType);
            this.eventService.insertOne(this.eventForm.value, multipartform).then(
                result => {
                    this.dataLoaded = false;
                    if (jQuery('#eventAvatar').val() != "" ){
                        this.uploader.onBuildItemForm = (item, form) => {
                            form.append("event", result.id);
                            form.append("description", 'eventPhoto');
                        };
                        this.uploadEventPic();
                    }
                    if (jQuery('#eventDocuments').val() != "" ) {
                        this.docsUploader.onBuildItemForm = (item, form) => {
                            form.append("event", result.id);
                            form.append("description", 'eventDocument');
                        };
                        this.uploadEventDocs();
                        this.docsQueueItem = 0;
                    }
                    jQuery('#newEventModal').openModal();
                    this.initDefaults();
                    Materialize.toast('Dein Event wurde erstellt', 4000, 'green rounded');
                }

            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.' + error, 4000, 'red rounded');
                    return false;
                }
            );

        } else {
            Materialize.toast('Bitte vervollständige oder korrigiere die markierten Felder', 4000, 'red');
        }
    }
}

