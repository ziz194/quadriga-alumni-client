import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IMAGE_URL, SERVER_URL } from '../../config';
import { SettingService } from '../shared/setting.service';
import { EventService } from '../shared/event.service';
import { MyFileUploader } from '../shared/my.file.uploader';
import { AuthService } from '../shared/auth.service';
import { CustomValidators } from '../shared/validators';
import { SET_CURRENT_USER_PROFILE, SETEVENTITEMS, SETEVENT, INCREMENT, DECREMENT, RESET, SETVAL , ADD_EVENT_BOOKMARK, DEL_EVENT_BOOKMARK, UPDATE_EVENTITEM } from '../shared/state.actions';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable}  from 'rxjs/Rx';
import { AppState,  IQAPUserProfile, IQAPEvent, IQAPApplicationDefault, IQAPEventItems, IQAPBookmarks } from '../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterializeDirective } from 'angular2-materialize';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventTypeSearch, EventRegionSearch, RemoveDuplicate } from '../shared/pipe.filters';
import { MomentModule } from 'angular2-moment';
import { SITE_TITLE } from '../../config';

@Component({
    selector: 'qap-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss'],
})

export class EventComponent {
    public events_error: Boolean = false;
    public qapeventitems: Observable<IQAPEventItems>;
    public event: Observable<IQAPEvent>;
    public qapeventbookmarks: Observable<IQAPBookmarks> ;
    public qapuserprofile: Observable<IQAPUserProfile>;
    private profile: IQAPUserProfile;
    public topicSearch = '';
    public regionSearch = '';
    private IMAGE_URL = IMAGE_URL;
    public events;
    public topicSearchDefault: string = 'Alle Themen';
    public regionSearchDefault: string = 'Region';
    public start_date_search: string = '';
    public end_date_search: string = '';

    constructor(public store: Store<AppState>,
                public eventService: EventService,
                private titleService: Title,
                private router: Router,
    ) {
        this.qapeventitems = store.select<IQAPEventItems>('qapeventitems');
        this.titleService.setTitle(SITE_TITLE);

        // this.qapeventbookmarks = store.select<IQAPBookmarks>('qceventbookmarks');
        this.store.select<IQAPUserProfile>('qapuserprofile').subscribe(
            data => {
                this.profile = data;
            }
        );
        this.getEvents();
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
    }

    public setTitle(newTitle: string) {
        //this.titleService.setTitle(newTitle);
    }

    private getEvents() {
        this.eventService.findAll().subscribe(
            data => {
                this.store.dispatch({ type: SETEVENTITEMS, payload: data });
            },
            err => { console.log(err); }
        );
    }
    filterTopic(search){
        this.topicSearch = search;
        if (this.topicSearch === ''){
            this.topicSearchDefault = 'Alle Themen';
        } else {
            this.topicSearchDefault = search;
        }
    }
    filterRegion(search){
        this.regionSearch = search;
        if (this.regionSearch === ''){
            this.regionSearchDefault = 'Region';
        } else {
            this.regionSearchDefault = search;
        }
    }
    filterDate() {
        let start_date_search_default = "01.01.1985" ;
        let end_date_search_default = '01.01.2085' ;
        let start_date_send = "" ;
        let end_date_send = "" ;

        if (this.start_date_search == ''){
            start_date_send = start_date_search_default ;
        }
        else start_date_send = this.start_date_search ;

        if (this.end_date_search == ''){
            end_date_send   = '01.01.2085' ;
        }
        else end_date_send = this.end_date_search ;
        this.eventService.findAllByDate(start_date_send, end_date_send).subscribe(
            data => {
                this.store.dispatch({ type: SETEVENTITEMS, payload: data });
            },
            err => { console.log(err); }
        );
    }

    toggleBookmark(event: IQAPEvent){
        console.log(event);
        if (event.isBookmarked){
            this.store.dispatch({ type: DEL_EVENT_BOOKMARK, payload: event.id });
        } else {
            this.store.dispatch({ type: ADD_EVENT_BOOKMARK, payload: event.id });
        }
        event.isBookmarked = !event.isBookmarked;
        this.store.dispatch({ type: UPDATE_EVENTITEM, payload: event });
    }
}