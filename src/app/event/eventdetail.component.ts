import { Component, Input, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Observable}  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { AppState, IQAPEvent, IQAPEventItems, IQAPUserProfile, IQAPComment, IQAPCommentItems, IQAPNewComment } from '../shared/interfaces'
import { SETEVENT, DEL_EVENT_BOOKMARK, ADD_EVENT_BOOKMARK, UPDATE_EVENTITEM, SET_CURRENT_USER_PROFILE } from '../shared/state.actions'
import { EventService } from '../shared/event.service';
import { IMAGE_URL } from '../../config';
import { Title } from '@angular/platform-browser';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventAttandeeIdSearch} from '../shared/pipe.filters'
import { MaterializeDirective } from "angular2-materialize";
import { CustomValidators } from '../shared/validators';
import { ProfileService } from '../shared/profile.service';
import { SITE_TITLE } from '../../config';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { SERVER_URL} from '../..//config';
import { DOCS_URL} from '../../config';
import { AuthGuardService } from '../shared/auth.guard.service';
import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/de';

const API_URL = SERVER_URL + 'uploadFile';

@Component({
      selector: 'qap-event-detail',
  templateUrl: './eventdetail.component.html', 
  styleUrls: ['./event.component.scss'],
})

export class EventDetailComponent {

    private mailForm: FormGroup;
    private commentForm: FormGroup;
    public qapeventitems: Observable<IQAPEventItems>;
    public event: Observable<IQAPEvent>;
    public qapuserprofile: Observable<IQAPUserProfile>;
    private profile: IQAPUserProfile;
    public qapcommentitems: Observable<IQAPCommentItems>;
    public events_error: Boolean = false;
    public notFound: Boolean = false;
    private isAttended: Boolean = false;
    public IMAGE_URL = IMAGE_URL;
    private SERVER_URL = SERVER_URL;
    public currentevent: IQAPEvent;
    public currentuser;
    public currentUserIsOwner: boolean = false;
    public currentevents: IQAPEventItems;
    private id: number;
    private cancel_user_id: number;
    public showComment: Boolean = true;
    private DOCS_URL = DOCS_URL;
    public eventPassed: boolean = false ; 


    constructor(
        public eventService: EventService,
        public store: Store<AppState>,
        private router: Router,
        private route: ActivatedRoute,
        private titleService: Title,
        private formBuilder: FormBuilder,
        private eventAttandeeIdSearch: EventAttandeeIdSearch,
        public profileService: ProfileService,
    ) {

        moment.locale('de');
        this.isAttended = false;
        window.scrollTo(0,0);
        this.event = this.store.select<IQAPEvent>('event');
        this.qapeventitems = store.select<IQAPEventItems>('qapeventitems');
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
        this.qapcommentitems = store.select<IQAPCommentItems>('qapcommentitems');
        this.isAttended = false;
        this.mailForm = formBuilder.group({
            subject: ['', Validators.required],
            message: ['', Validators.required]
        });

        this.commentForm = formBuilder.group({
            content: ['', Validators.required]
        });

        this.qapuserprofile.subscribe( data => 

            this.profileService.findOne(data.id).subscribe(
                data => {
                                this.profile = data;
                                this.cancel_user_id = this.profile.id ;
                            }
            //this.isAttended = this.eventAttandeeIdSearch.transform(this.currentevent.attendees, data.id);

        ));

        this.route.params.subscribe(params => {
            this.id = +params['id'];
            this.eventService.findOne(this.id)
                .subscribe(
                data => {
                    this.currentevent = data;
                    this.store.dispatch({ type: SETEVENT, payload: data })
                    this.titleService.setTitle(this.currentevent.title);
                    this.qapuserprofile.subscribe(data => {
                        if (this.currentevent.user.id === data.id) {
                            this.currentUserIsOwner = true;
                        }

                        //Check if the event is already passed
                        let eventEndDate = new Date(this.currentevent.endtime); 
                        if(eventEndDate <  new Date()){
                            this.eventPassed = true ; 
                        }
                      

                        this.profileService.findOne(data.id).subscribe(
                            data => {
                                this.profile = data;
                            }
                        );
                        this.isAttended = this.eventAttandeeIdSearch.transform(this.currentevent.attendees, data.id);

                    
                });

                },

                err => {
                    this.notFound = true;
                    Materialize.toast('Das Event konnte nicht gefunden werden', 4000, 'red');
                }
                )
        });

    }



    attendEvent() {

        if (this.isAttended === true) {

        }
        this.eventService.attend(this.id).then(
            data => {
                let result = data.json();
                Materialize.toast(result.message, 4000, 'green rounded');
                this.isAttended = true;
                this.eventService.findOne(this.id).subscribe(
                    data => {
                        this.currentevent = data;
                        this.store.dispatch({ type: SETEVENT, payload: data })
                    }
                );
            }).catch(
            error => {
                let data = error.json();
                Materialize.toast(data.error.message, 4000, 'red rounded');
            }
            )

    }


    showDeleteModal() {
        jQuery('#modalDeleteEvent').openModal();
    }

    deleteEvent(confirm: boolean) {
        if (confirm) {
            this.eventService.delete(this.id).then(
                result => {

                    jQuery('#modalDeleteEvent').closeModal();
                    Materialize.toast('Dein Event wurde gelöscht', 4000, 'green rounded');
                    this.router.navigate(['/events']);
                }
            ).catch(
                error => {
                    Materialize.toast('Ein Fehler ist aufgetreten.', 4000, 'red rounded')
                    return false;
                }
                );
        }
        else jQuery('#modalDeleteEvent').closeModal();

    }


        cancelAttendanceEvent(event: Event) {
         
        jQuery('#modalCancelAttendanceEvent').closeModal();
        this.eventService.cancelAttendance(this.id,this.cancel_user_id).then(

            data => {

                let result = data.json();
                this.isAttended = false;
                  
                Materialize.toast(result.message, 4000, 'green rounded');
                document.getElementById('attendee' +this.cancel_user_id.toString()).remove();

                this.eventService.findOne(this.id).subscribe(
                    data => {
                        this.currentevent = data;
                        this.store.dispatch({ type: SETEVENT, payload: data })
                    }
                );
                            }

        ).catch(

            error => {
                console.log('error', error);
                //let data = error.json();// data.error.message
                Materialize.toast('error', 4000, 'red rounded');
            }

            );
    }

    showcancelAttendanceEvent(event: Event, userid: number){
        jQuery('#modalCancelAttendanceEvent').openModal();
        this.cancel_user_id = userid;
        console.log (this.cancel_user_id)
    }
}