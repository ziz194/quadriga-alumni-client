import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileEditComponent } from './profile/profileedit.component';
import { AuthGuardService } from './shared/auth.guard.service';
import { RegisterComponent } from './login/register.component';
import { PasswordComponent } from './login/password.component';
import { EventComponent } from './event/event.component';
import { GroupComponent } from './group/group.component';
import { GroupNewComponent } from './group/groupnew.component';
import { GroupDetailComponent } from './group/groupdetail.component';
import { ProfileListComponent } from './profile/profilelist.component';
import { EventNewComponent } from './event/eventnew.component';
import { EventDetailComponent } from './event/eventdetail.component';
import { MessageComponent } from './message/message.component';
import { MessageNewComponent } from './message/messagenew.component';
import { MessageDetailComponent } from './message/messagedetail.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService]},
  { path: 'profile/edit', component: ProfileEditComponent, canActivate: [AuthGuardService]},
  { path: 'profile/:id', component: ProfileComponent, canActivate: [AuthGuardService]},
  { path: 'profile/:group', component: ProfileComponent, canActivate: [AuthGuardService]},
  { path: 'profiles', component: ProfileListComponent, canActivate: [AuthGuardService] },
  { path: 'register', component: RegisterComponent },
  { path: 'events', component: EventComponent , canActivate: [AuthGuardService]},
  { path: 'group', component: GroupComponent },
  { path: 'group/new', component: GroupNewComponent , canActivate: [AuthGuardService] },
  { path: 'group/:id', component: GroupDetailComponent, canActivate: [AuthGuardService] },
  { path: 'event/new', component: EventNewComponent, canActivate: [AuthGuardService]},
  { path: 'events/:id', component: EventDetailComponent, canActivate: [AuthGuardService] },
  { path: 'messages', component: MessageComponent , canActivate: [AuthGuardService]},
  { path: 'messages/new', component: MessageNewComponent, canActivate: [AuthGuardService] },
  { path: 'messages/:id', component: MessageDetailComponent, canActivate: [AuthGuardService] },
  { path: 'password', component: PasswordComponent},
  { path: '', component: LoginComponent}
    // { path: '**', redirectTo: 'home', pathMatch: 'full'}

];

export const routing = RouterModule.forRoot(routes);
