import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable}  from 'rxjs/Rx';
import { AppState, IQAPEvent, IQAPEventItems, IQAPUserProfile } from '../shared/interfaces';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { Router } from '@angular/router';
import { IMAGE_URL } from '../../config';


@Component({
  selector: 'qap-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  private selectedMenu : number;
  private event: Observable<IQAPEvent>;
  private qapeventitems: Observable<IQAPEventItems>;
  private qapuserprofile: Observable<IQAPUserProfile>;
  private IMAGE_URL: string ; 
  constructor(  
    private router: Router,
    public store: Store<AppState>,
  ) {
    this.selectedMenu = 0 ; 
    this.event = this.store.select<IQAPEvent>('event');
    this.IMAGE_URL = IMAGE_URL ;
    this.qapeventitems = store.select<IQAPEventItems>('qapeventitems');
    this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
  }
    logout() {
        this.router.navigate(['/']);
        localStorage.setItem('auth_token', '');
        localStorage.setItem('user', '');
        this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: "" });
    }
}
