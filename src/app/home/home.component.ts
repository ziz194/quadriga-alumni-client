import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute  } from '@angular/router';
import { RouteConfig, RouteParams, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, CanActivate} from 'angular2/router';
import { Observable }  from 'rxjs/Rx';
import { Store } from '@ngrx/store';
import { IMAGE_URL, SERVER_URL, FEED_URL } from '../../config';
import { ProfileService } from '../shared/profile.service';
import { EventService } from '../shared/event.service';
import { NewsFeedService } from '../shared/news.feed.service';
import { MessageService } from '../shared/message.service';
import { SITE_TITLE } from '../../config';
import { SET_CURRENT_USER_PROFILE, SETEVENTITEMS, SETEVENT, INCREMENT, DECREMENT, RESET, SETVAL , ADD_EVENT_BOOKMARK, DEL_EVENT_BOOKMARK, UPDATE_EVENTITEM } from '../shared/state.actions';
import { AppState,  IQAPUserProfile, IQAPEvent, IQAPApplicationDefault, IQAPEventItems, IQAPBookmarks } from '../shared/interfaces';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin, EventSearch, EventTopicSearch, EventTypeSearch } from '../shared/pipe.filters';

@Component({
    selector: 'qap-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],

})

export class HomeComponent implements OnInit {
    public events_error: Boolean = false;
    public qapeventitems: Observable<IQAPEventItems>;
    public event: Observable<IQAPEvent>;
    public qapeventbookmarks: Observable<IQAPBookmarks> ;
    public events;
    public qapuserprofile: Observable<IQAPUserProfile>;
    public isRequesting: boolean;
    public currentuser;
    private IMAGE_URL: string = IMAGE_URL;
    private SERVER_URL: string = SERVER_URL;
    private FEED_URL: string = FEED_URL;
    private feeds: any;
    


    constructor(
        private router: Router,
        private titleService: Title,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private profileService: ProfileService,
        private newsFeedService: NewsFeedService,
        public eventService: EventService,
    ) {
        this.titleService.setTitle(SITE_TITLE);
        this.qapeventitems = store.select<IQAPEventItems>('qapeventitems');
        this.qapuserprofile = store.select<IQAPUserProfile>('qapuserprofile');
        this.qapuserprofile.subscribe(
                data => {
                    this.profileService.findOne(data.id).subscribe(
                        data => {
                            this.currentuser = data;
                            this.titleService.setTitle('Inbox von ' + this.currentuser.firstname + ' ' + this.currentuser.lastname);
                        },
                    );
                }
        )
        this.getEvents();
        // this.titleService.setTitle('Hallo ' + this.currentuser.firstname + ' ' + this.currentuser.lastname);
    }

    ngOnInit() {
        jQuery('#sidebar').show() ;
        this.refreshFeed();
    }

    private getEvents() {
        this.eventService.findAll().subscribe(
            data => {
                this.store.dispatch({ type: SETEVENTITEMS, payload: data });
            },
            err => { console.log(err); }
        );
    }
    private refreshFeed() {
        this.isRequesting = true;
        this.newsFeedService.getFeedContent(this.FEED_URL)
            .subscribe(
                feed => this.feeds = feed.items,
                error => console.log(error));
        this.isRequesting = false;

    }
}
