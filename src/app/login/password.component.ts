import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers} from '@angular/http';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { Router, ActivatedRoute } from '@angular/router';
import { SERVER_URL} from '../../config';
import { Observable}  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState, IQAPUserProfile, } from '../shared/interfaces'
import { Title } from '@angular/platform-browser';
import { FilterPipe, CapitalizePipe, SplitJoin, SplitWrapJoin} from '../shared/pipe.filters'
import { MaterializeDirective } from "angular2-materialize";
import { CustomValidators } from '../shared/validators';
import { ProfileService } from '../shared/profile.service';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { AuthService } from '../shared/auth.service';



@Component({
  selector: 'qap-password',
  templateUrl: './password.component.html',
  styleUrls: ['./login.component.scss']
})

export class PasswordComponent {
    private passwordForm: FormGroup;
    private passwordResetForm;
    private token: string;
    private isSent: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState> ,
    private authService: AuthService
  ) {
    this.passwordForm = formBuilder.group({
            email: ['', Validators.required]
        });
        this.passwordResetForm = formBuilder.group({
            password: ['', Validators.required],
            password_repeat: ['', Validators.required]
        });
        console.log(this.token);
  }
    ngOnInit(): void {
         jQuery('#sidebar').hide() ; 
        this.route.params.subscribe(
            params => {
                this.token = params['token'];
                console.log(this.token);
            }
        );

        if (localStorage.getItem('user')) {
            this.router.navigate(['/events']);
        }
 
    }
    onSubmit() {   
        this.authService.sendResetMail(this.passwordForm.controls['email'].value).then(
            response => {
                this.isSent = true;
            }
        ).catch(
            error => {
                let data = error.json();
                console.log(data.error.message);
                if (data.error.message == "Email is not valid") {
                    Materialize.toast('Dies ist keine gültige Email.', 4000, 'red')
                } else {
                    this.isSent = true;
                }

            }
            );
    }
    onSubmitPassword() {
        this.token='test'; //comment
        console.log('test '+this.token); //comment
        if (this.passwordResetForm.controls['password'].value != this.passwordResetForm.controls['password_repeat'].value){
                 Materialize.toast('Passwörter stimmen nicht überein.', 4000, 'red')
        } else {
            
            this.authService.sendNewPassword(this.passwordResetForm.controls['password'].value, this.token).then(
                response => {
                    Materialize.toast('dein Passwort wurde gespeichert und du kannst dich jetzt damit einloggen', 4000, 'green')
                    this.router.navigate(['/login']);
                }
            ).catch(
                error => {
                    let data = error.json();
                    Materialize.toast(data.error.message, 4000, 'red')
                }
                );

        }
    }

}