import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../shared/interfaces';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../shared/auth.service';
@Component({
  selector: 'qap-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  private loginForm: FormGroup;
  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<AppState> 
  ) {
      this.loginForm = this.formBuilder.group({ 
        email: ['', Validators.required],
        password: ['', Validators.required] 
    });
  }

    ngOnInit() {
        jQuery('#sidebar').hide() ; 
        if (localStorage.getItem('user')) {
            this.router.navigate(['/home']);
        }
    }
 
    onSubmit() { 
        
        if (localStorage.getItem('auth_token') !== 'x') {
            let email = this.loginForm.controls['email'].value ; 
            let password =  this.loginForm.controls['password'].value ;  
                this.authService.authenticate(email, password)
                .subscribe(
                    data => {
                        localStorage.setItem('auth_token', data.json().token);
                        this.authService.user()
                        .subscribe(
                            data => {
                                this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data.json().user });
                                localStorage.setItem('user', data.json().user);
                                this.router.navigate(['/home']);
                            },
                            err => console.log('Error getting user from database: ' + err)
                        );
                    },
                    err => Materialize.toast(JSON.parse(err._body).error.message, 4000, 'rounded red')
                        // err => console.log(JSON.parse(err._body).error.message) 
                );
        }
    }
    
}


