import { Component } from '@angular/core';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers} from '@angular/http';
import { Router } from '@angular/router';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS} from 'angular2-jwt';
import { SERVER_URL} from '../../config';
import { LocalStorageService, LocalStorageSubscriber } from '@orkisz/angular2-localstorage/dist/LocalStorageEmitter';
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AppState, IQAPUserProfile } from '../shared/interfaces';
import { SET_CURRENT_USER_PROFILE } from '../shared/state.actions';
import { ProfileService } from '../shared/profile.service';
import { AuthService } from '../shared/auth.service';


@Component({
  selector: 'qap-register',
  templateUrl: './register.component.html',
  styleUrls: ['./login.component.scss']
})

export class RegisterComponent {
    private registerForm: FormGroup;
    constructor(

        private formBuilder: FormBuilder,
        private router: Router,
        public store: Store<AppState>,
        private profileService: ProfileService,
        private authService: AuthService

    ) {

        this.registerForm = formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

    }

    ngOnInit(): void {

        if (localStorage.getItem('user')) {
            this.router.navigate(['/home']);
        }

    }

    onSubmit() {
        //Add User To Database
        Materialize.toast('Dein Konto wird gespeichert...', 4000, 'rounded');
        let email = this.registerForm.controls['email'].value ; 
        let password =  this.registerForm.controls['password'].value ;       
        this.profileService.create(email, password).then(
            response => {
                let data = response.json();
        // Login User with the new Data
        if (localStorage.getItem('auth_token') !== 'x') {
                let email = this.registerForm.controls['email'].value ; 
                let password =  this.registerForm.controls['password'].value ;                  
                this.authService.authenticate(email,password)
                .subscribe(
                    data => {
                       localStorage.setItem('auth_token', data.json().token);
                        this.authService.user()
                        .subscribe(
                            data => {
                                this.store.dispatch({ type: SET_CURRENT_USER_PROFILE, payload: data.json().user });
                                let user = data.json().user;
                                jQuery('#modal1').openModal();
                                // this.router.navigate(['/profile/edit']);
                            },
                            err => console.log(err)
                            );

                    },
                    err => console.log('ERROR' + err)
                    );
            }
            }
        ).catch(
            error => {
                let data = error.json();
                Materialize.toast(data.error.message, 4000, 'red rounded')
            }
            );



    }

}