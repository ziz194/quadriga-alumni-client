import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/forms';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@ngrx/core';
import '@angular/router';
import 'materialize-css';
import 'materialize-css/sass';
import 'rxjs';
import 'hammerjs';
import 'es6-shim';
import '@angularclass/hmr';
import 'ngrx-store-localstorage';
import '@orkisz/angular2-localstorage';
import 'angular2-materialize';
import 'material-design-icons';
import 'ng2-file-upload';
import 'angular2-moment'; 
import 'ng2-pagination'; 
import 'moment'; 




// import 'rxjs/add/operator/toPromise';
// import 'rxjs/add/operator/map';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...
