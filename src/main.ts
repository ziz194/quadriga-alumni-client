import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { SERVER_URL} from './config'

export function main() {
  return platformBrowserDynamic().bootstrapModule(AppModule);
}
// depending on the env mode, enable prod mode or add debugging modules
if (process.env.ENV === 'production') {
  enableProdMode();
} else {
    console.log('QC ENV: ', process.env.ENV + ' ' + SERVER_URL);
}
if (document.readyState === 'complete') {
  main();
} else {
  document.addEventListener('DOMContentLoaded', main);
}
