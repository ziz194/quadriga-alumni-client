### Quick start

```bash
# clone the repo
$ git git@bitbucket.org:candeeland/alumni.quadriga-hochschule.com.git alumni-client

# change directory to your app
$ cd alumni-client

# install the dependencies with npm
$ npm install

# start the server
$ npm start
```

the app will run on [http://localhost:3000](http://localhost:3000) in your browser.
